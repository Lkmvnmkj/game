﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float jumpVelocity = 10f;

    private Rigidbody rigidBody;
    private int comboCounter;


    private void Awake()
    {
        comboCounter = 0;
        rigidBody = gameObject.GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Finish")
        {
            SceneManager.LoadScene("Level");
            return;
        }
        if (comboCounter > 2)
        {
            Destroy(collision.transform.parent.gameObject);
            PlatformManager.RemovePlatform(collision.transform.parent);
            rigidBody.velocity = Vector3.up * jumpVelocity;
            comboCounter = 0;
            return;
        }
        if (collision.collider.tag == "Obstacle")
        {
            rigidBody.velocity = Vector3.up * jumpVelocity;
        }
        if (collision.collider.tag == "DangerZone")
        {
            SceneManager.LoadScene("Level");
        }
        comboCounter = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        comboCounter++;
        Destroy(other.gameObject); 
        PlatformManager.RemovePlatform(other.transform);
    }
}
