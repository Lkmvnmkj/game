﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateLevel : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> prefabs;

    [SerializeField]
    private Transform parent;

    private float distanseBetweenPlatforms = 4f;

    void Awake()
    {
        for (int i = 50; i > 0; i--)
        {
            GameObject go = Instantiate(prefabs[Random.Range(1, 5)], new Vector3(0, 0, 0), Quaternion.identity);
            go.transform.parent = parent;
            go.transform.position = new Vector3(0, i * distanseBetweenPlatforms, 0);
            go.transform.Rotate(new Vector3(0, Random.Range(-180f, 180f), 0));
        }
    }
}