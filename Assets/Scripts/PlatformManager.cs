﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlatformManager : MonoBehaviour, IDragHandler
{
    [SerializeField]
    private GameObject obstackles;

    private static List<Transform> allChildren;

    private void Start()
    {
        allChildren = new List<Transform>();
        int childrenCount = obstackles.transform.childCount;
        for (int i = 0; i < childrenCount; i++)
        {
            allChildren.Add(obstackles.transform.GetChild(i));
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        foreach (Transform g in allChildren)
        {
            g.Rotate(0, -eventData.delta.x, 0);
        }
    }

    public static void RemovePlatform(Transform t)//removes first platform in List<Transform> allChildren;
    {
        allChildren.Remove(t);
    }
}
